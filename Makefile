
all: pattern-test
clean:
	rm -f pattern-test

pattern-test: pattern/pattern.c test/pattern_tables.c test/main.c
	gcc -O2 -o pattern-test -Ipattern -Itest pattern/pattern.c test/pattern_tables.c test/main.c

