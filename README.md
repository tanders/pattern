# Pattern

Simple \*/? pattern matcher. See [test/pattern_tables.c](test/pattern_tables.c) for examples of patterns

```c
int pattern(const unsigned char *text, int tlen, const unsigned char *pattern, int plen);
int pattern_n(const unsigned char *text, int tlen, const unsigned char *pattern, int plen);

// Returns 1 if the match succeeded, 0 otherwise
```

`make` creates the binary `pattern-test`:
```sh
./pattern-test -m test       # test for correctness
./pattern-test -m bench -i n # simple benchmark
```

