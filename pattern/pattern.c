#include <stdint.h>

#include "pattern.h"

unsigned char pattern_lower_tbl[] = {
    0,   1,   2,   3,   4,   5,   6,   7,   8,   9,
   10,  11,  12,  13,  14,  15,  16,  17,  18,  19,
   20,  21,  22,  23,  24,  25,  26,  27,  28,  29,
   30,  31,  32,  33,  34,  35,  36,  37,  38,  39,
   40,  41,  42,  43,  44,  45,  46,  47,  48,  49,
   50,  51,  52,  53,  54,  55,  56,  57,  58,  59,
   60,  61,  62,  63,  64, 'a', 'b', 'c', 'd', 'e',
  'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
  'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y',
  'z',  91,  92,  93,  94,  95,  96, 'a', 'b', 'c',
  'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
  'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
  'x', 'y', 'z', 123, 124, 125, 126, 127, 128, 129,
  130, 131, 132, 133, 134, 135, 136, 137, 138, 139,
  140, 141, 142, 143, 144, 145, 146, 147, 148, 149,
  150, 151, 152, 153, 154, 155, 156, 157, 158, 159,
  160, 161, 162, 163, 164, 165, 166, 167, 168, 169,
  170, 171, 172, 173, 174, 175, 176, 177, 178, 179,
  180, 181, 182, 183, 184, 185, 186, 187, 188, 189,
  190, 191, 192, 193, 194, 195, 196, 197, 198, 199,
  200, 201, 202, 203, 204, 205, 206, 207, 208, 209,
  210, 211, 212, 213, 214, 215, 216, 217, 218, 219,
  220, 221, 222, 223, 224, 225, 226, 227, 228, 229,
  230, 231, 232, 233, 234, 235, 236, 237, 238, 239,
  240, 241, 242, 243, 244, 245, 246, 247, 248, 249,
  250, 251, 252, 253, 254, 255
};

int pattern(const unsigned char *text, int tlen, const unsigned char *pattern, int plen)
{
  int   in_star = 0;              /* have we found a * yet? */

  unsigned char *g       = (unsigned char *)pattern; /* mask start ptr */
  unsigned char *t       = (unsigned char *)text;    /* str start ptr */
  unsigned char *gp      = g;             /* iterative ptr for mask */
  unsigned char *tp      = t;             /* iterative ptr for str */

  /* we leave when we hit the end of text */
  while (*tp != 0)
  {
    switch (*gp)
    {
      case '*':
        /* go to next char in pattern */
        gp++;

        /* the * is at the end of pattern, we're done */
        if (*gp == 0)
        {
          return 1;
        }

        /* rememeber we found a * for later */
        in_star = 1;

        /* advance the "main" pointers to our current pos and break */
        t = tp;
        g = gp;
        break;

      default:
        if ((*tp & ~32) != (*gp & ~32))
        {
          /* if we haven't hit a * yet, we fail */
          if (in_star == 0)
          {
            return 0;
          }

          /* otherwise rewind to our last known "good" position */
          tp = ++t;
          gp = g;
          break;
        }

        /* go into the next case if we fail this if */

      case '?':
        /* increment the pattern and text pointers */
        gp++;
        tp++;
    }
  }

  /* skip any trailing *s if necessary */
  while (*gp == '*')
  {
    gp++;
  }

  /* if we made it to the end of the pattern, we're good (since we're also at the
     end of the text). Otherwise, we fail. */
  return (*gp == 0);
}

/* safer version than the one above that actually checks plen */
int pattern_n(const unsigned char *text, int tlen, const unsigned char *pattern, int plen) {
  uint8_t   *gp = (uint8_t *)pattern;
  uint8_t   *sp = (uint8_t *)text;

  uintptr_t  gn = plen;
  uintptr_t  sn = tlen;

  uintptr_t  st = 0;
  uintptr_t  gi = 0;
  uintptr_t  si = 0;

  uintptr_t  g  = gi;
  uintptr_t  s  = si;

  while (s < sn && g <= gn) {
    if (gp[g] == '*') {
      if (g + 1 >= gn) return 1;
      st = 1;
      g += 1;
      si = s;
      gi = g;
    } else {
      if (gp[g] != '?' && (gp[g] & ~32) != (sp[s] & ~32)) {
        if (st == 0) return 0;
        si += 1;
        s = si;
        g = gi;
      } else {
        g++; s++;
      }
    }
  }

  while (g < gn && gp[g] == '*') g++;

  return (g == gn);
}
