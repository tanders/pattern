#ifndef _PATTERN_H
#define _PATTERN_H

#include <sys/types.h>

__BEGIN_DECLS

#define PATTERN_TOLOWER(y) pattern_lower_tbl[(y)]

extern unsigned char pattern_lower_tbl[];

int pattern(const unsigned char *text, int tlen, const unsigned char *pattern, int plen);
int pattern_n(const unsigned char *text, int tlen, const unsigned char *pattern, int plen);

__END_DECLS

#endif /* _PATTERN_H */

/* end of pattern.h */
