#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "pattern.h"
#include "pattern_tables.h"

typedef struct match_method_s
{
  char            *name;  /* the name of this match method */
  float            time;  /* time taken to run the benchmark */

  int              (*fn)(const unsigned char *, int, const unsigned char *, int);
                   /* function that implements the match algorithm */
} match_method_t;

match_method_t match_methods[] = {
  {"P0", 0.0f, pattern  },
  {"PN", 0.0f, pattern_n},
  {NULL, 0.0f, NULL     }
};

/*  glob_benchmark
 *
 *  Author: Travis Anderson <tanderson@codeshare.ca>
 *  Author: James Couzens <jcouzens@codeshare.ca>
 *
 *  Date:   09/17/04
 *
 *  Desc:   Benchmarks each matching method against the data in
 *          pattern_tables.c; 'iterations' is the number of times
 *          each method will be ran against the data set.
 *
 */
int glob_benchmark(uint32_t iterations, int quiet)
{
  int i                = 0;
  int j                = 0;
  int table_size       = 0;
  uint32_t iters       = 0;
  pattern_table_t *p   = NULL;
  pattern_table_t *tbl = NULL;
  match_method_t *mm   = NULL;
  struct timeval start = {0};
  struct timeval end   = {0};
  float fastest        = 100000000.0f; /* dont start off too "fast" */

  if (iterations == 0)
  {
    iterations = 1;
  }

  iters = iterations;
  mm = match_methods;
  tbl = pattern_table_bench;

  for (i = 0; mm[i].name != NULL; i++)
  {
    table_size = 0;

    /* calculate the size of this set. a bit of kludge, but eh... */
    for (p = tbl; p->pattern != NULL; p++)
    {
      table_size++;
    }

    if (!quiet) {    
      printf("  Running %s on %ix%d (%d) string/pattern pairs... ",
             mm[i].name, table_size, iters, (iters * table_size));

      fflush(stdout);
    }

    /* record the time of the start of the loop */
    gettimeofday(&start, NULL);

    for (j = 0; j < iters; j++)
    {
      for (p = tbl; p->pattern != NULL; p++)
      {
        mm[i].fn(p->text, strlen(p->text), p->pattern, strlen(p->pattern));
      }
    }

    /* record the time of the end of the loop */
    gettimeofday(&end, NULL);

    /* calculate this method's elapsed time */
    mm[i].time = ((float)(end.tv_sec  - start.tv_sec)  +
                 ((float)(end.tv_usec - start.tv_usec) / 1000000));

    /* was it the fastest so far? */
    fastest = (mm[i].time < fastest) ? mm[i].time : fastest;

    if (!quiet) {
      printf("Done.\n");
    }
  }

  printf("\n");

  /* spit out some handy stats */
  for (i = 0; mm[i].fn != NULL; i++)
  {
    printf("  Result: %s: %0.6f secs (%0.3f pairs/s, %0.3f weight)\n",
            mm[i].name, mm[i].time, ((float)(table_size * iters) / mm[i].time),
           (mm[i].time / fastest));
  }

  printf("\n");

  return 1;
}


/*  glob_test
 *
 *  Author: Travis Anderson <tanderson@codeshare.ca>
 *  Author: James Couzens <jcouzens@codeshare.ca>
 *
 *  Date:   09/17/04
 *
 *  Desc:   Tests the algorithms in match.c/h against the data in
 *          pattern_tables.c/h for correctness.
 *
 */
int glob_test(void)
{
  int i                = 0;    /* iterator */
  int res              = 0;    /* result */
  int testtotal        = 1;    /* utility counter relating to test number */
  int errortotal       = 0;    /* error count */
  pattern_table_t *p   = NULL; /* pattern table pointer */
  pattern_table_t *tbl = NULL;
  match_method_t *mm   = NULL; /* pointer to the match_methods global */

  mm = match_methods;
  tbl = pattern_table_test;

  for (i = 0; mm[i].name != NULL; i++)
  {
    testtotal  = 0;
    errortotal = 0;

    printf("*** Testing: %s\n", mm[i].name);

    for (p = tbl; p->pattern != NULL; p++, testtotal++)
    {
      res = mm[i].fn(p->text, strlen(p->text), p->pattern, strlen(p->pattern));

      if (res != p->expect)
      {
        printf("  [%i]: %s: (%s), String: (%s).  Expected %i, received %i\n",
          testtotal,
          mm[i].name,
          p->pattern ? (char *)p->pattern : "NULL",
          p->text    ? (char *)p->text    : "NULL",
          p->expect, res);

        errortotal++;
      }
    } /* for */

    printf("*** Failed: %i/%i", errortotal, testtotal);

    if (errortotal == 0)
    {
      printf(" - All %i comparisons resulted in success\n\n", testtotal);
    }
    else
    {
      printf("\n\n");
    }
  } /* for */

  return(1);
}


/*  print_usage
 *
 *  Author: Travis Anderson <tanderson@codeshare.ca>
 *
 *  Date:   09/17/04
 *
 *  Desc:   Prints usage instructions to the user.
 *
 */
void print_usage(char *arg)
{
  printf("usage: %s -m <mode> [-i <iterations>]\n", arg);
  printf("  modes:\n");
  printf("    test  - test correctness of the various implementations\n");
  printf("    bench - benchmark the various implementations\n");
  printf("  iterations:\n");
  printf("    n     - specifies how many times to loop during a benchmark\n");
  printf("\n");
}


/*  main
 *
 *  Author: Travis Anderson <tanderson@codeshare.ca>
 *
 *  Date:   09/17/04
 *
 *  Desc:   Program entry point
 *
 */
int main(int argc, char **argv)
{
  int       i          = 0;
  char     *p          = NULL;
  char     *mode       = NULL;
  uint32_t  iterations = 0;

  if (argc < 2)
  {
    printf("Too few arguments\n");
    print_usage(argv[0]);
    return 1;
  }

  for (i = 1; i < argc; i += 2)
  {
    p = argv[i];

    if (*p != '-')
    {
      printf("Invalid arguments\n");
      print_usage(argv[0]);
      return 1;
    }

    p++;

    switch (*p)
    {
      case 'm':
        mode = argv[i + 1];
        break;

      case 'i':
        iterations = atoi(argv[i + 1]);
        break;

      default:
        printf("Unrecognized argument: '%c'\n", *p);
        print_usage(argv[0]);
        return 1;
    }
  }

  if (iterations == 0)
  {
    iterations = 1;
  }

  if (strcasecmp(mode, "test") == 0)
  {
    glob_test();
  }
  else if (strcasecmp(mode, "bench") == 0)
  {
    glob_benchmark(iterations, 1);
    glob_benchmark(iterations, 0);
  }
  else
  {
    printf("Unrecognized mode: '%s'\n", mode);
    print_usage(argv[0]);
    return 1;
  }

  return 0;
}

/* end main.c */
