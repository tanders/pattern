#ifndef _PATTERN_TABLES_H
#define _PATTERN_TABLES_H 1


#include <sys/types.h>    /* standard types */


__BEGIN_DECLS


typedef struct pattern_table_s
{
  unsigned char *text;    /* string for comparison */
  unsigned char *pattern; /* regular expression */

  int   expect;  /* expected result fo matching mask against str */
} pattern_table_t;


extern pattern_table_t pattern_table_test[];
extern pattern_table_t pattern_table_bench[];


__END_DECLS


#endif /* _PATTERN_TABLES_H */

/* end of pattern_tables.h */
